use std::time::Duration;
use rand::{self, Rng};
use crate::game;
use crate::game::{Piece,Loc,Board};
use crate::state;

pub struct UCT{
    shared: state::State,
    rand_device: rand::rngs::ThreadRng,
}

impl UCT{
    pub fn new(shared:state::State) -> UCT {
        UCT {
            shared,
            rand_device: rand::thread_rng(),
        }
    }
    pub fn step(&mut self, me_piece:Piece, time_usable:Duration) -> Option<Loc> {
        let now_board = self.shared.now_board();
        let empty_locs = game::board_empty_locs(&now_board);
        if empty_locs.is_empty() {
            None
        } else {
            let loc = self.rand_device.gen_range(0..empty_locs.len());
            Some(empty_locs[loc])
        }
    }
}
