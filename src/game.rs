// Lianstar-RS by clonne/2023
// Game Base Structures

pub const BOARD_LEN:usize = 225;
pub const BOARD_SIZE:usize = 15;

pub type Loc = usize;
pub fn hv_to_loc(h:Loc, v:Loc) -> Loc {
    h + (v * (BOARD_SIZE as Loc))
}
pub fn loc_to_hv(loc:Loc) -> (Loc,Loc) {
    (loc % BOARD_SIZE, loc / BOARD_SIZE)
}

pub type Piece = i8;
pub const P_BLACK:Piece = -1;
pub const P_EMPTY:Piece = 0;
pub const P_WHITE:Piece = 1;

pub type Board = [Piece; BOARD_LEN];

pub fn board_make_empty() -> Board {
    [P_EMPTY; BOARD_LEN]
}

pub fn board_make(from:&Board) -> Board {
    *from
}

pub fn board_empty_locs(from:&Board) -> Vec<Loc> {
    let mut locs:Vec<Loc> = Vec::new();
    for i in 0..from.len() {
        if from[i] == P_EMPTY {
            locs.push(i as Loc);
        }
    }
    locs
}