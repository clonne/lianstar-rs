from PyQt6.QtWidgets import QWidget
from PyQt6.QtGui import QColor,QFont
from PyQt6.QtCore import Qt

__QSS = """
* {
    font-family: "Microsoft YaHei";
    font-size: 16pt;
}

QLabel#A {
    background-color: green;
}

QLabel#B {
    background-color: gray;
}

QLabel#gamestatus-label {
    qproperty-alignment:AlignCenter;
    background-color: #666666;
    color: #cccccc;
    font-weight: bold;
}

QLabel#gamestatus-text {
    qproperty-alignment:AlignCenter;
    background-color: #555555;
    color: #bbbbbb;
    font-size: 14pt;
}

QLabel#gamestatus-text-timer-now {
    qproperty-alignment:AlignCenter;
    background-color: #555555;
    color: #dddddd;
    font-weight: bold;
    font-size: 14pt;
}

QPushButton#tabs-log-clear {
    background-color: #3030aa;
    color: #dddddd;
}
QPushButton#tabs-log-clear:hover {
    color: #eeeeee;
    font-weight: bold;
}

QTextEdit#tabs-log-text {
    font-size: 14pt;
}
"""

__COLOR = {
    "gamecanvas-bg": Qt.GlobalColor.darkGray,
    "gameboard-bg": QColor(255,180,91),
    "gameboard-coord": QColor(17,17,17),
    "gameboard-star": QColor(32,32,32),
    "gamepiece-black-border": QColor(10,10,10),
    "gamepiece-black-bg": Qt.GlobalColor.black,
    "gamepiece-white-border": QColor(10,10,10),
    "gamepiece-white-bg": Qt.GlobalColor.white,
    "gamestep-black": QColor(238,238,238),
    "gamestep-white": QColor(17,17,17),
}

__FONT = {
    "gameboard-coord": QFont("Courier New", 12),
    "gamestep": QFont("Arial", 14),
}

def uxtheme_color(id:str) -> QColor:
    return __COLOR.get(id, Qt.GlobalColor.black)

def uxtheme_font(id:str) -> QFont:
    return __FONT.get(id, QFont())

def uxtheme_use(window:QWidget, id:str="default") -> None:
    window.setStyleSheet(__QSS)
