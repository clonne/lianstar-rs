# 连星五子棋RS版
* 本程序为个人实验性质的五子棋AI平台
* 提供一个通用界面和内置AI引擎
* 界面与AI引擎使用进程管道通信，可使用所有支持[连星RS协议](PROTOCOL.md)的AI引擎
* by clonne/2023

## 简单的个人作品
连星RS版的前身为起始于2021的连星WEB版：桌面/移动端通用的网页五子棋AI对战 [Gitee](https://clonne.gitee.io/webapps/?-lianstar)<br>
由于博弈类AI属于高密度运算，在浏览器中性能受限，就算是用WASM也好不到哪去，所以决定再开启一个桌面版。<br>

